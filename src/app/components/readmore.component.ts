import {Component, OnInit, Input, AfterViewInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'readmore',
    templateUrl: './readmore.component.html',
    styleUrls: ['./readmore.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class ReadmoreComponent implements OnInit,AfterViewInit{
    @Input('length') length: number;
    @Input('text') text:string;
    
    container: HTMLElement;
    anchorMore: HTMLElement;
    anchorLess: HTMLElement;
    hidden: boolean;
    constructor(){
        
    }

    ngOnInit(){
        this.hidden = false;
    }    

    ngAfterViewInit(){
        this.container = document.querySelector('#container');
        console.log('container',this.container);
        if(this.text.length > this.length){
            let sub = this.text.substring(0, this.length);
            sub+= '<span class="hidden">';
            sub+= this.text.substring(this.length, this.text.length);
            sub+= '</span>';            
            sub+= '<a id="more" href="" (click)="more($event)">...Read more</a> ';
            sub+= '<a id="less" class="hidden" href="" (click)="less($event)">Read less</a> ';
            this.text = sub;            
        }
        this.container.innerHTML = this.text;
        this.hidden = true;
    }

    check(){
        this.anchorMore = document.querySelector('#more');
        this.anchorLess = document.querySelector('#less');        
        this.anchorMore.classList.toggle('hidden');
        this.anchorLess.classList.add('hidden');
    }
    



    less(){

    }

    more(e){
        e.preventDefault();
        
        console.log('container',this.container);
        
    }

    
}